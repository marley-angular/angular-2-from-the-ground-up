# [Udemy] Angular 2 From The Ground Up [2016, ENG]

<br/>

### 01 First Application

<br/>

**01 Writing a Component**

![Application](/img/theme_01_pic_01.png?raw=true)


<br/>

**02 Composing Components**

![Application](/img/theme_01_pic_02.png?raw=true)


<br/>

**03 Writing a Service**


<br/>

**04 Dependency Injection**


    var MockQuoteService = Class({
        constructor: function(){},
        getRandomQuote: function(){
            return {
                line: 'A mock quote.',
                author: 'Mock Author'
            };
        }
    });

<br/>


providers: [
           { provide: QuoteService, useClass: QuoteService }
 ],

<br/>

 providers: [
           { provide: QuoteService, useClass: MockQuoteService }
  ],

<br/>

  providers: [
         { provide: QuoteService, useValue: new MockQuoteService() }
    ],

<br/>

    providers: [
        { provide: QuoteService, useFactory: function(){
            return new MockQuoteService();
        }}
      ],


<br/>

**05 Change Detection**

<br/>

**06 File Modules**


<br/>

### 02 ES6/ES2015

<br/>

**01 ES6 Specification**

<br/>

**02 Project Setup with NPM and Babel**

    $ cd 02_ES6_ES2015
    $ npm install
    $ npm run build
    $ npm run serve

http://localhost:3000/


<br/>

**03 Multiline Strings Arrow Functions Let and Const**

<br/>

**04 Classes Decorators**

<br/>

**05 ES6 Modules**



<br/>

### 03 TypeScript

<br/>

**01 TypeScript Language**

    $ cd 03_TypeScript
    $ npm install
    $ npm run build
    $ npm run serve

http://localhost:3000/

<br/>

**02 Type Annotations**

<br/>

**03 Private Modifier**

<br/>

### 04 Templates

<br/>

**01 Property Binding**

    $ cd 04_Templates/
    $ npm install
    $ npm run build
    $ npm run serve


![Application](/img/theme_04_pic_01.png?raw=true)


<br/>

**02 Properties vs Attributesg**

<br/>

**02 Properties vs Attributesg**

<br/>

**03 Event Binding**

![Application](/img/theme_04_pic_02.png?raw=true)

<br/>

    <input id="baseAmountField" type="number" value={{baseAmount}} (input)="update($event.target.value)">

<br/>

    update(baseAmount){
        this.targetAmount = parseFloat(baseAmount) * this.exchangeRate;
    }



![Application](/img/theme_04_pic_03.png?raw=true)


<br/>

**04 Two-Way Binding**

    Convert: <input type="number" [(ngModel)]="baseAmount"> USD

![Application](/img/theme_04_pic_04.png?raw=true)


<br/>

**05 Class and Style Binding**

    [ngClass]="{error: isInvalid(baseAmount)}"


<br/>

**06 Helper Service**

![Application](/img/theme_04_pic_05.png?raw=true)


<br/>

**07 Component Property Binding**

![Application](/img/theme_04_pic_06.png?raw=true)

<br/>

**08 ngFor Directive**

<br/>

**09 Component Event and Two-Way Binding**

<br/>

**10 ngIf Directive**

<br/>

**11 Built-In Pipes**

<br/>

**12 Writing a Pipe**

<br/>

### 05 Form Validation

<br/>

**01 HTML5 Form Validation**

    $ cd 05_Form_Validation/
    $ npm install
    $ npm run serve

http://localhost:3000

![Application](/img/theme_05_pic_01.png?raw=true)


<br/>

**02 ng-validng-invalid**

![Application](/img/theme_05_pic_02.png?raw=true)


<br/>

**03 Validation Messages**

![Application](/img/theme_05_pic_03.png?raw=true)

![Application](/img/theme_05_pic_04.png?raw=true)


<br/>

**04 NgForm**

![Application](/img/theme_05_pic_05.png?raw=true)


<br/>

**05 Writing a Directive**

![Application](/img/theme_05_pic_06.png?raw=true)

<br/>

**06 Directive Custom Events**

![Application](/img/theme_05_pic_07.png?raw=true)


<br/>

### 06 HTTP Client and Backend Integration

<br/>

**01 Using the HTTP Client**

    $ cd 06_HTTP_Client_and_Backend_Integration/
    $ npm install
    $ npm run serve


http://localhost:3000/quote.json

![Application](/img/theme_06_pic_01.png?raw=true)

http://localhost:3000

![Application](/img/theme_06_pic_02.png?raw=true)


<br/>

**02 ES6 Promises**

![Application](/img/theme_06_pic_03.png?raw=true)


<br/>

**03 Rx Observables**

http://reactivex.io/rxjs/


![Application](/img/theme_06_pic_04.png?raw=true)

<br/>

**04 REST APIs and Firebase**

https://firebase.google.com/

Create a project: angular2-course

DataBase:

Rules:

    {
      "rules": {
        ".read": "true",
        ".write": "true"
      }
    }


Postman:

POST: https://angular2-course-cfc92.firebaseio.com/bookmarks.json

Body --> JSON

    {
        "title": "Angular 2",
        "url": "https://angular.io"
    }

<br/>

    {
        "title": "RxJS",
        "url": "https://reactivex.io/rxjs/"
    }

<br/>

    {
        "title": "Firebase",
        "url": "https://firebase.google.com"
    }



<br/>

**05 Retrieving Data**

    $ npm install
    $ npm run serve


![Application](/img/theme_06_pic_05.png?raw=true)


![Application](/img/theme_06_pic_06.png?raw=true)


<br/>

**06 Creating Data**

![Application](/img/theme_06_pic_07.png?raw=true)


<br/>

**07 Deleting Data**

![Application](/img/theme_06_pic_08.png?raw=true)


<br/>

**08 Updating Data**

![Application](/img/theme_06_pic_09.png?raw=true)

<br/>

**09 Error Handling**

![Application](/img/theme_06_pic_10.png?raw=true)


<br/>

### 07 Production-Ready Build Workflow

<br/>

**01 Build Workflow Requirements**

![Application](/img/theme_07_pic_1.png?raw=true)

<br/>

**02 Angular CLI**

    # npm install -g angular-cli
    $ cd 07_Production_Ready_Build_Workflow/
    $ ng init --name "production_ready_build_workflow"
    $ ng serve --host 0.0.0.0 --port 3000

<br/>

    $ ng -v
    $ ng build -dev
    $ ng build -prod
    $ ng test


Next Videos:  
https://github.com/marley-angular/Angular-2-with-Webpack-Project-Setup


<br/>

### 08 Routing

<br/>

**01 Angular Tunes**

    $ cd 08_Routing
    $ npm install
    $ npm run serve


![Application](/img/theme_08_pic_01.png?raw=true)

<br/>

**02 Navigation with ngSwitch** (Not usable, only for example)

![Application](/img/theme_08_pic_02.png?raw=true)

<br/>

**03 Angular Router**

    $ cd 08_Routing
    $ npm install
    $ npm run serve


<br/>

**04 Route Parameters**

<br/>

**05 Guards**


![Application](/img/theme_08_pic_03.png?raw=true)

![Application](/img/theme_08_pic_04.png?raw=true)


<br/>

**06 Query Parameters**

<br/>

**07 HTML5 History API**


    $ cd 09_Unit_Testing_with_Jasmine_and_Karma
    $ npm install
    $ npm run serve

<br/>

### 09 Unit Testing with Jasmine and Karma

<br/>

**01 Guess The Word**

<br/>

**02 Jasmine Tests**

    $ npm install --save-dev jasmine-core

<br/>

Browser:

    file:///projects/dev/Angular-2-From-The-Ground-Up/09_Unit_Testing_with_Jasmine_and_Karma/src/jasmine.html

<br/>

![Application](/img/theme_09_pic_01.png?raw=true)


<br/>

**03 Karma Test Runner**

    $(npm bin)/typings install --save --global dt~jasmine

<br/>

    $ npm install --save-dev karma karma-jasmine karma-webpack karma-chrome-launcher

<br/>

    $ $(npm bin)/karma --version
    Karma version: 1.3.0

    $ $(npm bin)/karma start --port 3000

    $ $(npm bin)/karma start --port 3000 --single-run (not works for me)

    $ npm run test




<br/>
<br/>
___

**Marley**

<a href="https://jsdev.org">jsdev.org</a>

email:  
![Marley](http://img.fotografii.org/a3333333mail.gif "Marley")
