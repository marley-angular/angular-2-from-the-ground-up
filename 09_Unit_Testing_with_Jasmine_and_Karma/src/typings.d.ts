declare var app: {
  environment: string;
};

interface WebpackRequire {
    (id: string): any;
    context(dir: string, useSubdirs: boolean, pattrn: RegExp): any;
}

declare var require: WebpackRequire;
