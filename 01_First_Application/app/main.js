(function(app){

    var platformBrowserDynamic = ng.platformBrowserDynamic.platformBrowserDynamic;

    var QuoteService = app.QuoteService;
    var RandomQuoteComponent = app.RandomQuoteComponent;
    var AppComponent = app.AppComponent;
    var AppModule = app.AppModule;

    platformBrowserDynamic().bootstrapModule(AppModule);

})(window.app || (window.app = {}));
